---
layout: page
title: About
class: 'post'
navigation: True
logo: 'assets/images/ghost.png'
current: about
---

This is a demo blog for Ghost, it contains some dummy content which allows you to click around and see what a Ghost blog running the default theme looks like.

We use this for testing and for reference!

This Jekyll Theme was configured by @virtuacreative to be hosted by [GitLab.com](https://about.gitlab.com/) with [GitLab Pages](http://doc.gitlab.com/ee/pages/README.html). [Fork it](https://gitlab.com/jekyll-themes/jasper)!

View original source on [GitHub](https://github.com/biomadeira/jasper).

If you'd like to set up your own blog, head on over to [https://ghost.org](https://ghost.org) and sign up.
